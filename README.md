config
======

Package config simplifies the code for an application to handle configurations.

* It parses flag `config` for an config file path or a remote etcd URL.
* If `config` flag is not set, it searches locally for an configuration file:
    - .<app>/config.yml
    - $HOME/.<app>/config.yml
    - /etc/<app>/config.yml
    - where <app> is the file name of the executable.
* The config file must contain an entry to configure `appcoachs.net/x/log`

    ```
    log:
        release:   1.0
        mode:      production
        formatter: logstash
    ```
