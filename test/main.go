package main

import (
	"fmt"

	"appcoachs.net/x/config"
	"appcoachs.net/x/log"
)

type conf struct {
	X string
	Y int
}

func main() {
	var conf conf
	config.Parse(&conf)
	fmt.Printf("%#v\n", conf)
	fmt.Println("formatter:", log.GetFormatter())
	fmt.Println("release:", log.GetRelease())
	fmt.Println("mode:", log.GetMode())
}
