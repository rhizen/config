package config // import "pkg.cocoad.mobi/x/config"

import (
	"flag"
	"net/url"
	"os"
	"path"

	"github.com/spf13/viper"
	"pkg.cocoad.mobi/x/log"
)

// Load loads configuration file from local path or remote ETCD server.
// The default path is local:
// 1. .<app>/config.yml
// 2. $HOME/.<app>/config.yml
// 3. /etc/<app>/config.yml
// where <app> is the file name of the executable.
func Parse(config interface{}) (err error) {
	var configFile string
	flag.StringVar(&configFile, "config", "", "local config file or etcd remote URL, e.g. http://127.0.0.1:4001/app/config/config.yml")
	if !flag.Parsed() {
		flag.Parse()
	}
	return ParseFile(configFile, config)
}

func ParseFile(configFile string, config interface{}) error {
	viper.SetConfigType("yml")
	// set log option before any logging
	if err := load(configFile); err != nil {
		return err
	}
	if err := setLogOptions(); err != nil {
		return err
	}
	return unmarshal(config)
}
func isURL(path string) bool {
	u, err := url.Parse(path)
	return err == nil && u.Scheme != ""
}

func load(configFile string) error {
	if configFile == "" {
		return loadLocal()
	}
	if isURL(configFile) {
		return loadRemote(configFile)
	}
	viper.SetConfigFile(configFile)
	return viper.ReadInConfig()
}

func loadLocal() error {
	dir, name := path.Base(os.Args[0]), "config"
	viper.SetConfigName(name)
	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME/." + dir)
	viper.AddConfigPath("/etc/" + dir)
	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	log.Debug("local config loaded")
	return nil
}

func loadRemote(remoteConfigURL string) error {
	endpoint, path, err := parseURL(remoteConfigURL)
	if err != nil {
		return err
	}
	viper.AddRemoteProvider("etcd", endpoint, path)
	if err := viper.ReadRemoteConfig(); err != nil {
		return err
	}
	log.Fields{
		"url": remoteConfigURL,
	}.Debug("remote config loaded")
	return nil
}
func parseURL(remote string) (endpoint, path string, _ error) {
	uri, err := url.Parse(remote)
	if err != nil {
		return "", "", err
	}
	path = uri.Path
	uri.Path = ""
	endpoint = uri.String()
	return
}

func unmarshal(config interface{}) error {
	if err := viper.Unmarshal(config); err != nil {
		return err
	}
	log.Fields{
		"config": config,
	}.Debug("config read")
	return nil
}

func setLogOptions() error {
	var l struct {
		Log struct {
			Mode      string
			Level     string
			Formatter string
			Release   string
			Writer    struct {
				Type string
			}
		}
	}
	if err := unmarshal(&l); err != nil {
		return err
	}
	log.SetFormatter(l.Log.Formatter) // set formatter before any logging
	log.SetMode(l.Log.Mode)
	log.SetLevel(l.Log.Level)
	log.SetRelease(l.Log.Release)
	switch l.Log.Writer.Type {
	default:
		log.Debug("default to stdout writer")
	}
	log.Fields{
		"log config": l.Log,
	}.Debug("set log config")
	log.StartDaemon()
	return nil
}
